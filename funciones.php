<?php

function dameListaRecetas($userSession){
	
	try {
		//conectar a bases de datos
		$conn = new PDO('sqlite:recetario.sqlite');
	
		$listado = '
				SELECT *, re.id AS id_receta, usu.id AS id_usuario, usu.username
			     	FROM  recetas re
			      	JOIN  auth_user usu
			      	ON (re.usuario_id = usu.id)
					ORDER BY fecha_modificacion DESC
			      ;
				';
                
		$resultado = $conn->query($listado);
	
		foreach($resultado as $re){
			$registros[] = $re;
		}
		
		if (empty($registros)) {
			$registros="";
		}
	
	} 
	catch(PDOException $e){
		echo $e->getMessage();
	} 
	
	// cierra conexion
	$conn = null;
	
	
	//renderizar plantilla
	$datos = array(
			'listarecetas' => $registros,
			'userSession' => $userSession
	);
	return $datos;
}
function dameResultadoBusqueda($userSession, $nomReceta, $tipoReceta, $username){
	
	try {
		//conectar a bases de datos
		$conn = new PDO('sqlite:recetario.sqlite');
	
		$consulta = $conn->prepare('
									SELECT *, usu.id AS id_usuario, usu.username
								     	FROM  recetas re
								      	JOIN  auth_user usu ON (re.usuario_id = usu.id)
										WHERE re.nombre_receta LIKE :nomRe
											AND re.tipo_receta LIKE :tipoRe
											AND usu.username LIKE :usu
										ORDER BY fecha_modificacion DESC;
									'
								);
		
		
		$consulta->bindParam(":nomRe", $nomRe);
		$consulta->bindParam(":tipoRe", $tipRe);
		$consulta->bindParam(":usu", $usu);
		
		$nomRe = "%".$nomReceta."%";
		$tipRe = "%".$tipoReceta."%";
		$usu = "%".$username."%";
		
		$consulta->execute();
		
		$registros = $consulta->fetchAll(PDO::FETCH_ASSOC);
	
	} 
	catch(PDOException $e){
		echo $e->getMessage();
	} 
	
	$conn = null;
	
	//renderizar plantilla
	$datos = array(
			'listarecetas' => $registros,
			'userSession' => $userSession
	);
	return $datos;
	
}



function dameFechaActual(){
	date_default_timezone_set('Europe/Madrid');
	$resultado = strftime( "%Y-%m-%d-%H-%M-%S", time() );
	return $resultado;
}



function añadeReceta($idusu, $imagen, $nomReceta, $tipoPlato, $ingredientes, $elaboracion, $fechaCre, $fechaMod, $tiempo){
	
	try {
		$conn = new PDO('sqlite:recetario.sqlite');
		$consulta = $conn->prepare('
									INSERT INTO recetas (usuario_id, tipo_receta, nombre_receta, elaboracion, ingredientes, fecha_creacion, fecha_modificacion, tiempo_elaboracion, imagen ) 
										VALUES (
												:idusu,
												:tip,
												:nomrec,
												:elab,
												:ing,
												:fechac,
												:fecham,
												:tiem,
												:img
												);
								   '
								);
		$consulta->bindParam(":idusu", $idusu);
		$consulta->bindParam(":tip", $tip);
		$consulta->bindParam(":nomrec", $nomrec);
		$consulta->bindParam(":elab", $elab);
		$consulta->bindParam(":ing", $ing);
		$consulta->bindParam(":fechac", $fechac);
		$consulta->bindParam(":fecham", $fecham);
		$consulta->bindParam(":tiem", $tiem);
		$consulta->bindParam(":img", $img);
		
		$idusu = $idusu; 
		$img = $imagen;
		$nomrec = $nomReceta;
		$tip = $tipoPlato;
		$ing = $ingredientes;
		$elab = $elaboracion;
		$fechac = $fechaCre;
		$fecham = $fechaMod;
		$tiem = $tiempo;
		
		$consulta->execute();
	
	}
	catch(PDOException $e){
		echo $e->getMessage();
	}
	// cierra conexion
	$conn = null;
	

}
function dameRecetaPorId($userSession, $idrec){
	
	try {
		//conectar a bases de datos
		$conn = new PDO('sqlite:recetario.sqlite');
		$consulta = $conn->prepare('
									SELECT *, re.id AS id_receta,  usu.id AS id_usuario, usu.username
										FROM  recetas re
										JOIN  auth_user usu
										ON (re.usuario_id = usu.id)
										WHERE re.id=:idrec
										;
									'
									);
		$consulta->bindParam(":idrec", $id_re);
		$id_re = $idrec;
		$consulta->execute();
		$registros = $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	catch(PDOException $e){
		echo $e->getMessage();
	} 
	
	// cierra conexion
	$conn = null;
	
	$fechaActual = dameFechaActual();
	$datos = array(
			'camposreceta' => $registros,
			'fechaActual' => $fechaActual,
			'userSession' => $userSession
	);
	return $datos;
}

function editaReceta($idre, $idusu, $imagen, $nomReceta, $tipoPlato, $ingredientes, $elaboracion, $fechaCre, $fechaMod, $tiempo){
	try {
		//conectar a bases de datos
		$conn = new PDO('sqlite:recetario.sqlite');
		$consulta = $conn->prepare('
									UPDATE recetas
										SET 
											nombre_receta =:nomrec, 
											tipo_receta =:tip, 
											ingredientes =:ing, 
											elaboracion =:elab,
											fecha_modificacion =:fecham, 
											tiempo_elaboracion =:tiem, 
											imagen =:img
										WHERE recetas.id=:idre; 
								   '
								);
		$consulta->bindParam(":nomrec", $nomrec);
		$consulta->bindParam(":tip", $tip);
		$consulta->bindParam(":ing", $ing);
		$consulta->bindParam(":elab", $elab);
		$consulta->bindParam(":fecham", $fecham);
		$consulta->bindParam(":tiem", $tiem);
		$consulta->bindParam(":img", $img);
		$consulta->bindParam(":idre", $id_re);
		
		$nomrec = $nomReceta;
		$tip = $tipoPlato;
		$ing = $ingredientes;
		$elab = $elaboracion;
		$fecham = $fechaMod;
		$tiem = $tiempo;
		$img = $imagen;
		$id_re = $idre;
		
		$consulta->execute();
	
	}
	catch(PDOException $e){
		echo $e->getMessage();
	} 
	// cierra conexion
	$conn = null;
}

function eliminarReceta($idrec){
	
	try {
		//conectar a bases de datos
		$conn = new PDO('sqlite:recetario.sqlite');
		$consulta = $conn->prepare('
									DELETE FROM recetas WHERE recetas.id=:idrec;
									'
									);
		$consulta->bindParam(":idrec", $id_re);
		
		$id_re = $idrec;
		$consulta->execute();
		
		$registros = $consulta->fetchAll(PDO::FETCH_ASSOC);
	} 
	catch(PDOException $e){
		echo $e->getMessage();
	} 
	
	// cierra conexion
	$conn = null;
	
	$datos = array(
			'camposreceta' => $registros,
			'userSession' => $userSession
	);
	return $datos;
}


function convertirPass($pass){
	
	include 'lib/PasswordHash.php';
	
	$contrasena1 = create_hash($pass);
	$contrasena2 = str_replace(':', '$', $contrasena1); 
	$correct_hash = 'pbkdf2_'.$contrasena2;
	
	if (validate_password($pass, $correct_hash)){
		echo 'Contraseña Correcta';
	}
	else {
		echo 'Contraseña incorrecta';
	}
	return $correct_hash;
}

function comprobarUserPass($usu, $pass){
	
	$pass = 'idoia';
	convertirPass($pass);
	
	$valido = false;
	
	if (empty($usu) || empty($pass)) {	
		$valido = false;
	}else {

		$passDjango = convertirPass($pass);
		
		try {
			//conectar a bases de datos
			$conn = new PDO('sqlite:recetario.sqlite');
			$consulta = $conn->prepare('
											SELECT username, password 
												FROM auth_user 
												WHERE username=:us AND password=:psw
											;									
										'
									  );
		
			$consulta->bindParam(":us", $us);
			$consulta->bindParam(":psw", $psw);
		
			$us = $usu;
			$psw = $pass;
			
			$consulta->execute();

			$registros = $consulta->fetchAll(PDO::FETCH_ASSOC);
			
			if (!empty($registros)) {
				$valido = true;
			}else {
				$valido = false;
			}
		
		}
		catch(PDOException $e){
		echo $e->getMessage();
		} 
		
		// cierra conexion
		$conn = null;
		
		

	$datos = array(
		'camposreceta' => $registros,
		'userSession' => $userSession
	);
		
		
	
	}
	return $valido;
	
}

function muestraAviso($aviso, $link){
	
	include 'lib/config.php';
	$template = $twig->loadTemplate("aviso.html");
	
	$datos = array(
					'aviso' => $aviso,
					'link' => $link
					);
	echo $template->render($datos);
			
}
?>
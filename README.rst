Instalación
===========

Clonar el siguiente repositorio:

::

	git clone https://martafg28@bitbucket.org/martafg28/recetariophp.git


Instalar motor de plantillas TWIG
=================================

Por consola:

- Descargar 'composer':

::

	cd /ruta/recetasphp/
		curl -s http://getcomposer.org/installer | php	
	
- Creamos un archivo llamado 'composer.json' en /ruta/recetasphp/  y escribimos en el:

::

	{
   		"require": {
       		"twig/twig": "1.*"
   		}
 	}
	
	
- Instalamos composer:

::
	
	php composer.phar install




Base de datos
============================

Utilizamos la base de datos recetario.sqlite


Abrir aplicación
=================

::

	localhost/recetariophp


Funcionalidad:
--------------
	- Aplicación creada en PHP con gestión de templates con Twig.
	- Gestiona una base de datos sqlite.
	- Se utilizan sesiones de usuarios mediante COOKIES.
	- Se gestiona la creación, edición y eliminación de recetas.
	- Cada usuario solo podrá editar y/o eliminar sus recetas.
	- De cada receta se gestiona: el nombre, el tipo (entrate, primero,	segundo o postre), las fechas de creación y modificación, el tiempo de elaboración, la elaboración y una imagen.
	






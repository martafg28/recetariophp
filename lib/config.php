<?php

// Datos de configuración


// Datos del administrador
$root = "root";
$password_admin = "root";


// configuración twig
require_once realpath(dirname(__FILE__).'/../vendor/twig/twig/lib/Twig/Autoloader.php');
// clase de twig.

Twig_Autoloader::register();


// instancia del cargador
$loader = new Twig_Loader_Filesystem(realpath(dirname(__FILE__).'/../templates'));	

// motor de plantillas
$twig = new Twig_Environment($loader);


?>
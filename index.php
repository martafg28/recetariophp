<?php

session_start(); 

if (empty($_SESSION['user'])) {		
 	$_SESSION['user']='invitado';	
}
$userSession = $_SESSION['user'];

include 'lib/config.php';

//template
$template = $twig->loadTemplate("index.html");

//logica
include_once 'funciones.php';

//datos
$datos = dameListaRecetas($userSession);

//Enviamos los datos a la plantilla
echo $template->render($datos);
?>
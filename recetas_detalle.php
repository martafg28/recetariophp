<?php
session_start();
$id_re = $_GET['id_receta'];
include 'lib/config.php';
$template = $twig->loadTemplate("recetas_detalle.html");

//logica
try {
	// conectar a bases de datos
	$conn = new PDO('sqlite:appRecetario_receta.db');
	$consulta = ' 
				SELECT *, usu.id AS id_usuario, usu.username
			     	FROM  recetas
			      	JOIN  auth_user usu
			      	ON (recetas.usuario_id = usu.id)
					WHERE recetas.id =:idre
			      ;
				';
	
	$consulta->bindParam(':idre', $idre);
	
	$idre = $id_re;
	
	$consulta->execute();
	
	$camposreceta = $consulta->fetchAll(PDO::FETCH_ASSOC);
}
catch(PDOException $e){
	echo $e->getMessage();
}
// cierra conexion
$conn = null;

//renderizar plantilla
$datos = array(
				'camposreceta' => $camposreceta,
				'userSession' => $userSession,
				);
echo $template->render($datos);

?>







